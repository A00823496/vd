﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pDead : MonoBehaviour
{

    public bool dead = false;
    private Rigidbody2D rb;
    Animator animator;
    public float waitTime = 1.75f;
    private float timer = 0.0f;
    private float initTime = 0.0f;
    public GameObject DeathSound;
    private bool death=false;
    public int cont = 0;
 
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "Enemy1")
        {
            dead = true;

            initTime = timer;
            this.GetComponent<pMovement>().enabled = false;
            this.GetComponent<pJump>().enabled = false;
            if (death == false && cont==0 )
            {
                Instantiate(DeathSound);
                cont++;
            }

        }
    }

    private void FixedUpdate()
    {
        //animator.SetBool("dead", dead);
        timer += Time.deltaTime;
        animator.SetBool("dead", dead);
        if (dead == true & (timer - initTime) > waitTime)
        {
            dead = false;
            this.GetComponent<pMovement>().enabled = true;
            this.GetComponent<pJump>().enabled = true;
        }
    }
    void Update()
    {
        
    }

}
